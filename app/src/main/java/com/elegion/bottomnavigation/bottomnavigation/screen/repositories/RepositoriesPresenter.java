package com.elegion.bottomnavigation.bottomnavigation.screen.repositories;

import android.support.annotation.NonNull;

import com.elegion.bottomnavigation.bottomnavigation.R;
import com.elegion.bottomnavigation.bottomnavigation.content.Repository;
import com.elegion.bottomnavigation.bottomnavigation.repository.RepositoryProvider;
import com.elegion.bottomnavigation.bottomnavigation.rx.loader.LifecycleHandler;

/**
 * @author Artur Vasilov
 */
public class RepositoriesPresenter {

    private final LifecycleHandler mLifecycleHandler;
    private final RepositoriesView mView;

    public RepositoriesPresenter(@NonNull LifecycleHandler lifecycleHandler,
                                 @NonNull RepositoriesView view) {
        mLifecycleHandler = lifecycleHandler;
        mView = view;
    }

    public void init() {
        RepositoryProvider.provideGithubRepository()
                .repositories()
                .doOnSubscribe(mView::showLoading)
                .doOnTerminate(mView::hideLoading)
                .compose(mLifecycleHandler.load(R.id.repositories_request))
                .subscribe(mView::showRepositories, throwable -> mView.showError());
    }

    public void onItemClick(@NonNull Repository repository) {
        mView.showCommits(repository);
    }
}
