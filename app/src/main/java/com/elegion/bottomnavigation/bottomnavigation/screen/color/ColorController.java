package com.elegion.bottomnavigation.bottomnavigation.screen.color;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.elegion.bottomnavigation.bottomnavigation.R;
import com.elegion.bottomnavigation.bottomnavigation.base.controllers.BaseController;

import butterknife.BindView;

/**
 * @author Valiev Timur
 */
public class ColorController extends BaseController {

    public static final String KEY_INDEX = "index";
    public static final String PARENT_CONTROLLER = "parent_controller";

    @BindView(R.id.tvTitle) TextView mTitleTextView;

    private int mIndex;

    private String mParentControllerTitle;

    public ColorController(@NonNull Bundle args) {
        super(args);
        mIndex = args.getInt(KEY_INDEX);
        mParentControllerTitle = args.getString(PARENT_CONTROLLER);
    }

    @Override
    protected View inflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container) {
        return inflater.inflate(R.layout.c_color, container, false);
    }

    @Override
    protected void onViewBound(@NonNull View view) {
        super.onViewBound(view);
        view.setBackgroundColor(getMaterialColor(getResources(), mIndex));
        mTitleTextView.setText(getResources().getString(R.string.color_controller_message,
                mParentControllerTitle, mIndex));
    }

    public int getMaterialColor(Resources resources, int index) {
        TypedArray colors = resources.obtainTypedArray(R.array.mdcolor_300);

        final int returnColor = colors.getColor(index % colors.length(), Color.BLACK);

        colors.recycle();
        return returnColor;
    }

}
