package com.elegion.bottomnavigation.bottomnavigation.screen.auth;

import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.bluelinelabs.conductor.RouterTransaction;
import com.elegion.bottomnavigation.bottomnavigation.R;
import com.elegion.bottomnavigation.bottomnavigation.base.controllers.BaseController;
import com.elegion.bottomnavigation.bottomnavigation.rx.loader.LifecycleHandler;
import com.elegion.bottomnavigation.bottomnavigation.rx.loader.LoaderLifecycleHandler;
import com.elegion.bottomnavigation.bottomnavigation.screen.general.LoadingView;
import com.elegion.bottomnavigation.bottomnavigation.screen.repositories.RepositoriesController;

import butterknife.BindView;
import butterknife.OnClick;

public class AuthController extends BaseController implements AuthView {

    @BindView(R.id.loginEdit)
    EditText mLoginEdit;

    @BindView(R.id.passwordEdit)
    EditText mPasswordEdit;

    @BindView(R.id.loginInputLayout)
    TextInputLayout mLoginInputLayout;

    @BindView(R.id.passwordInputLayout)
    TextInputLayout mPasswordInputLayout;

    private LoadingView mLoadingView;

    private AuthPresenter mPresenter;

    @Override
    protected View inflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container) {
        return inflater.inflate(R.layout.c_github_auth, container, false);
    }

    @Override
    protected void onViewBound(@NonNull View view) {
        super.onViewBound(view);
//        mLoadingView = LoadingDialog.view(getSupportFragmentManager());
        LifecycleHandler lifecycleHandler = LoaderLifecycleHandler.create(getActivity(),
                getSupportLoaderManager());
        mPresenter = new AuthPresenter(lifecycleHandler, this);
        mPresenter.init();
    }

    @Override
    protected int getTitle() {
        return R.string.toolbar_auth;
    }

    @SuppressWarnings("unused")
    @OnClick(R.id.logInButton)
    public void onLogInButtonClick() {
        String login = mLoginEdit.getText().toString();
        String password = mPasswordEdit.getText().toString();
        mPresenter.tryLogIn(login, password);
    }

    @Override
    public void showLoading() {
//        mLoadingView.showLoading();
    }

    @Override
    public void hideLoading() {
//        mLoadingView.hideLoading();
    }

    @Override
    public void showLoginError() {
        mLoginInputLayout.setError(getString(R.string.error));
    }

    @Override
    public void showPasswordError() {
        mPasswordInputLayout.setError(getString(R.string.error));
    }

    @Override
    public void openRepositoriesScreen() {
        getRouter().setRoot(RouterTransaction.with(new RepositoriesController()));
    }
}
