package com.elegion.bottomnavigation.bottomnavigation;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.SparseArray;
import android.view.MenuItem;
import android.view.ViewGroup;

import com.bluelinelabs.conductor.Conductor;
import com.bluelinelabs.conductor.Router;
import com.bluelinelabs.conductor.RouterTransaction;
import com.elegion.bottomnavigation.bottomnavigation.screen.auth.AuthController;
import com.elegion.bottomnavigation.bottomnavigation.screen.info.InfoController;
import com.elegion.bottomnavigation.bottomnavigation.screen.transition.TransitionController;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements
        BottomNavigationView.OnNavigationItemSelectedListener,
        ActionBarProvider {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.controllerContainer)
    ViewGroup mContainer;

    @BindView(R.id.bottomNavigationView)
    BottomNavigationView mBottomNavigationView;

    private int mCurrentItemId;

    private Router mRouter;

    private SparseArray<List<RouterTransaction>> mBackStacks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_main);

        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        mBottomNavigationView.setOnNavigationItemSelectedListener(this);

        mBackStacks = new SparseArray<>();
        mRouter = Conductor.attachRouter(this, mContainer, savedInstanceState);
        if (!mRouter.hasRootController()) {
            mRouter.setRoot(RouterTransaction.with(new AuthController()));
        }
    }

    @Override
    public void onBackPressed() {
        if (!mRouter.handleBack()) {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (mCurrentItemId == item.getItemId()) {
            return false;
        }

        mBackStacks.remove(mCurrentItemId);
        mBackStacks.put(mCurrentItemId, mRouter.getBackstack());

        mCurrentItemId = item.getItemId();

        if (mBackStacks.get(mCurrentItemId) != null) {
            mRouter.setBackstack(mBackStacks.get(mCurrentItemId), null);
            return true;
        }

        switch (item.getItemId()) {
            case R.id.action_github:
                mRouter.setRoot(RouterTransaction.with(new AuthController()));
                break;
            case R.id.action_info:
                mRouter.setRoot(RouterTransaction.with(new InfoController()));
                break;
            case R.id.action_transition:
                mRouter.setRoot(RouterTransaction.with(new TransitionController(TransitionController.putIndexToBundle(0))));
                break;
        }
        return true;
    }

}
