package com.elegion.bottomnavigation.bottomnavigation.base.controllers;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.app.LoaderManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.bluelinelabs.conductor.Controller;
import com.elegion.bottomnavigation.bottomnavigation.ActionBarProvider;

import static com.elegion.bottomnavigation.bottomnavigation.utils.AndroidUtils.checkNotNull;

public abstract class BaseController extends RefWatchingController {

    protected BaseController() {
    }

    protected BaseController(Bundle args) {
        super(args);
    }

    protected ActionBar getActionBar() {
        ActionBarProvider actionBarProvider = ((ActionBarProvider) getActivity());
        return actionBarProvider != null ? actionBarProvider.getSupportActionBar() : null;
    }

    @Override
    protected void onAttach(@NonNull View view) {
        setTitle();
        super.onAttach(view);
    }

    protected void setTitle() {
        Controller parentController = getParentController();
        while (parentController != null) {
            if (parentController instanceof BaseController && ((BaseController) parentController).getTitle() != 0) {
                return;
            }
            parentController = parentController.getParentController();
        }

        @StringRes int title = getTitle();
        ActionBar actionBar = getActionBar();
        if (title != 0 && actionBar != null) {
            actionBar.setTitle(getTitle());
        }
    }

    @NonNull
    protected LoaderManager getSupportLoaderManager() {
        return checkNotNull((AppCompatActivity) getActivity()).getSupportLoaderManager();
    }

    @NonNull
    protected String getString(@StringRes int stringRes) {
        return checkNotNull((AppCompatActivity) getActivity()).getString(stringRes);
    }

    @StringRes
    protected int getTitle() {
        return 0;
    }
}
