package com.elegion.bottomnavigation.bottomnavigation.controllers;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.elegion.bottomnavigation.bottomnavigation.R;
import com.elegion.bottomnavigation.bottomnavigation.base.controllers.BaseController;

/**
 * @author Valiev Timur
 */
public class MessagesController extends BaseController {

    @Override
    protected View inflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container) {
        return inflater.inflate(R.layout.c_messages, container, false);
    }

    @Override
    protected int getTitle() {
        return R.string.bottom_navigation_transition;
    }
}
