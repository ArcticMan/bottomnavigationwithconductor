package com.elegion.bottomnavigation.bottomnavigation.screen.auth;


import com.elegion.bottomnavigation.bottomnavigation.screen.general.LoadingView;

/**
 * @author Artur Vasilov
 */
public interface AuthView extends LoadingView {

    void openRepositoriesScreen();

    void showLoginError();

    void showPasswordError();

}