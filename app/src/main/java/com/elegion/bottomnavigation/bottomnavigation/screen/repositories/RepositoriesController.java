package com.elegion.bottomnavigation.bottomnavigation.screen.repositories;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.elegion.bottomnavigation.bottomnavigation.R;
import com.elegion.bottomnavigation.bottomnavigation.content.Repository;
import com.elegion.bottomnavigation.bottomnavigation.base.controllers.BaseController;
import com.elegion.bottomnavigation.bottomnavigation.rx.loader.LifecycleHandler;
import com.elegion.bottomnavigation.bottomnavigation.rx.loader.LoaderLifecycleHandler;
import com.elegion.bottomnavigation.bottomnavigation.screen.general.LoadingView;
import com.elegion.bottomnavigation.bottomnavigation.widget.BaseAdapter;
import com.elegion.bottomnavigation.bottomnavigation.widget.DividerItemDecoration;
import com.elegion.bottomnavigation.bottomnavigation.widget.EmptyRecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * @author Artur Vasilov
 */
public class RepositoriesController extends BaseController implements RepositoriesView,
        BaseAdapter.OnItemClickListener<Repository> {

    @BindView(R.id.recyclerView)
    EmptyRecyclerView mRecyclerView;

    @BindView(R.id.empty)
    View mEmptyView;

    private LoadingView mLoadingView;

    private RepositoriesAdapter mAdapter;

    private RepositoriesPresenter mPresenter;

    @Override
    protected View inflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container) {
        return inflater.inflate(R.layout.c_repositories, container, false);
    }

    @Override
    protected void onViewBound(@NonNull View view) {
        super.onViewBound(view);

//        mLoadingView = LoadingDialog.view(getSupportFragmentManager());

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        mRecyclerView.setEmptyView(mEmptyView);

        mAdapter = new RepositoriesAdapter(new ArrayList<>());
        mAdapter.attachToRecyclerView(mRecyclerView);
        mAdapter.setOnItemClickListener(this);

        LifecycleHandler lifecycleHandler = LoaderLifecycleHandler.create(getActivity(), getSupportLoaderManager());
        mPresenter = new RepositoriesPresenter(lifecycleHandler, this);
        mPresenter.init();
    }

    @Override
    protected int getTitle() {
        return R.string.toolbar_github;
    }


    @Override
    public void onItemClick(@NonNull Repository item) {
        mPresenter.onItemClick(item);
    }

    @Override
    public void showRepositories(@NonNull List<Repository> repositories) {
        mAdapter.changeDataSet(repositories);
    }

    @Override
    public void showCommits(@NonNull Repository repository) {
        // TODO: show commits
    }

    @Override
    public void showLoading() {
//        mLoadingView.showLoading();
    }

    @Override
    public void hideLoading() {
//        mLoadingView.hideLoading();
    }

    @Override
    public void showError() {
        mAdapter.clear();
    }
}
