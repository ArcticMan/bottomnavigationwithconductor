package com.elegion.bottomnavigation.bottomnavigation;

import android.support.v7.app.ActionBar;

public interface ActionBarProvider {
    ActionBar getSupportActionBar();
}
