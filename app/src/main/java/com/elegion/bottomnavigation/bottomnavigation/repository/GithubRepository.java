package com.elegion.bottomnavigation.bottomnavigation.repository;

import android.support.annotation.NonNull;

import com.elegion.bottomnavigation.bottomnavigation.content.Authorization;
import com.elegion.bottomnavigation.bottomnavigation.content.Repository;

import java.util.List;

import rx.Observable;

/**
 * @author Artur Vasilov
 */
public interface GithubRepository {

    @NonNull
    Observable<List<Repository>> repositories();

    @NonNull
    Observable<Authorization> auth(@NonNull String login, @NonNull String password);
}
