package com.elegion.bottomnavigation.bottomnavigation.screen.general;

/**
 * @author Artur Vasilov
 */
public interface LoadingView {

    void showLoading();

    void hideLoading();

}