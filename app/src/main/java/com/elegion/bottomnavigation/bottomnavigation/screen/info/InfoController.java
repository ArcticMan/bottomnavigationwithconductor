package com.elegion.bottomnavigation.bottomnavigation.screen.info;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bluelinelabs.conductor.RouterTransaction;
import com.elegion.bottomnavigation.bottomnavigation.R;
import com.elegion.bottomnavigation.bottomnavigation.base.controllers.BaseController;
import com.elegion.bottomnavigation.bottomnavigation.screen.color.ColorController;

import butterknife.OnClick;

/**
 * @author Valiev Timur
 */
public class InfoController extends BaseController {

    @Override
    protected View inflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container) {
        return inflater.inflate(R.layout.c_info, container, false);
    }

    @Override
    protected void onViewBound(@NonNull View view) {
        super.onViewBound(view);
        view.setOnClickListener(v -> {
            Bundle args1 = new Bundle();
            args1.putInt(ColorController.KEY_INDEX, getRouter().getBackstackSize());
            args1.putString(ColorController.PARENT_CONTROLLER, InfoController.class.getSimpleName());
            getRouter().pushController(RouterTransaction.with(new ColorController(args1)));

        });
    }

    @Override
    protected int getTitle() {
        return R.string.bottom_navigation_info;
    }

}
