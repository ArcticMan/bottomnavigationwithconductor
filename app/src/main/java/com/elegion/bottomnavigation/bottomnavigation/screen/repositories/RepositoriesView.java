package com.elegion.bottomnavigation.bottomnavigation.screen.repositories;

import android.support.annotation.NonNull;

import com.elegion.bottomnavigation.bottomnavigation.content.Repository;
import com.elegion.bottomnavigation.bottomnavigation.screen.general.LoadingView;

import java.util.List;


/**
 * @author Artur Vasilov
 */
public interface RepositoriesView extends LoadingView {

    void showRepositories(@NonNull List<Repository> repositories);

    void showCommits(@NonNull Repository repository);

    void showError();
}
